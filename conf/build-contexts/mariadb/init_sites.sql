-- SQL MARIADB init script --





CREATE DATABASE IF NOT EXISTS keycloak;

CREATE USER IF NOT EXISTS 'keycloak'@'keycloak.plapi-net-1' IDENTIFIED BY 'keycloakpass';
-- ALTER USER 'keycloak'@'keycloak.plapi-net-1' IDENTIFIED BY 'keycloakpass';
-- SELECT user, host FROM mysql.user;

GRANT ALL PRIVILEGES ON keycloak.* TO 'keycloak'@'keycloak.plapi-net-1';


FLUSH PRIVILEGES;