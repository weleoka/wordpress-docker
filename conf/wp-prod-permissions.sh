#!/bin/bash
#
# Simple permissions switching for Wordpress sites.
#
# author: Weleoka <weleoka@mailfence.com>
# date: 2021-01-29
#
# You will need root priviledges. Parameters are path to the root 
# of a wordpress installation, and the user to own the files as. 
# Optionally also set the group, bt there the default will be www-data.
#
# This script sets wordpress in production mode. It is based on the 
# hardening wordpress best practices. The principal is that everything
# is read-only for the PHP user except what has to be written to (uploads and cache).
#
# Content will be modifiable through web interface but not updates etc.
#
# https://wordpress.org/support/article/hardening-wordpress/
# 
# Optional special treatment for .htaccess? For rewrite rules in Apache to be created
# dynamically by Wordpress it may be a good idea to have .htaccess writeable by www-data.


# Bash strict mode
# -e exit on non zerow return value from any command.
# -u exit if reference is made to a variable which isn't previously defined. 
# -o pipefail, return non zero if any of the commands in the pipe have non zerow, not only the last one.
# IFS = Internal Field Seperator (delimiter) as newlines and tab characters, default IFS=$' \n\t'.
set -euo pipefail
IFS=$'\n\t '

# Absolute path of this script, usefull if it has relative dependencies.
script="`realpath "$0"`"
script_dir="`dirname "$script"`"

# Param: Absolute path to working dir
work_dir=${1:-""}
username=${2:-www-data}
groupname=${3:-www-data}

# Check path param
if ! [ -n "$work_dir" ]; then
  echo "Invalid param: usage 'wp-permissions-xx.sh ![path-to-wordpress] ![username] ?[groupname]'" >&2
  exit 1 
fi
# Check is dir
if ! [ -d ${work_dir} ]; then
  echo "Directory '${work_dir}' is not valid. Aborting." >&2
  exit 1
fi 

# Check username param (-n means: if not null)
set +u # Disable bash's null var check
if [ -n "$2" ]; then
  if ! [ -n "$3" ]; then
    echo "No groupname (arg3) specified: defaulting to 'www-data'"
  fi
else
  echo "No username (arg1) specified. Aborting." >&2
  exit 1
fi
set -u # re-enable bash's null var check

echo
echo "Setting PROD permissions for Wordpress installed at:"
echo "'${work_dir}'"
echo "user and group: '${username}:${groupname}'"
echo

echo "### Step 1: chown files and dirs to ${username}:${groupname}"
chown "${username}:${groupname}" -R "${work_dir}"/*
echo

echo "### Step 2: chown uploads and cache dirs 'www-data'"
sudo chown -R www-data:www-data "${work_dir}/wp-content/uploads" # Let apache be owner of wp-content/uploads
sudo chown -R www-data:www-data "${work_dir}/wp-content/cache" # Let apache be owner of wp-content/cache
echo

echo "### Step 3: chmod dirs to rwxr-x---, excluding root dir"
find "${work_dir}" -mindepth 1 -type d -exec chmod 750 {} \;  
echo

echo "### Step 4: chmod files to rw-r-----"
find "${work_dir}" -type f -exec chmod 640 {} \;
echo

echo "Done! Please double check to see if things went according to plan."
echo "Thanks for using this script!"
echo